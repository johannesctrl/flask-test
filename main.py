from flask import Flask, render_template

app = Flask(__name__)


@app.route('/', methods=["GET"])
def index():
    return "Greetings from GeeksforGeeks"


if __name__ == '__main__':
    app.run(debug=False, host="0.0.0.0", port=12347)
